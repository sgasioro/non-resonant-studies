#!/usr/bin/env python3

from pull_imp_utils import *
import pyhf
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import json
import os

from argparse import ArgumentParser

def main(ws_file, doPulls, doCorrelations, doImpacts):
    ws = pyhf.Workspace(json.load(open(ws_file)))

    if doPulls:
        pulls, pullerr, labels = calc_pulls(ws) 

        plt.hlines([-2, 2], -0.5, len(pulls) - 0.5, colors="k", linestyles="dotted")
        plt.hlines([-1, 1], -0.5, len(pulls) - 0.5, colors="k", linestyles="dashdot")
        plt.fill_between([-0.5, len(pulls) - 0.5], [-2, -2], [2, 2], facecolor="yellow")
        plt.fill_between([-0.5, len(pulls) - 0.5], [-1, -1], [1, 1], facecolor="green")
        plt.hlines([0], -0.5, len(pulls) - 0.5, colors="k", linestyles="dashed")
        plt.scatter(range(len(pulls)), pulls, c="k")
        plt.errorbar(
            range(len(pulls)), pulls, c="k", xerr=0, yerr=pullerr, marker=".", fmt="none"
        )
        plt.xticks(range(len(pulls)), labels, rotation=90)
        plt.gcf().set_size_inches(20, 5)
        plt.xlim(-0.5, len(pulls) - 0.5)
        plt.title("Pull Plot", fontsize=18)
        plt.ylabel(r"$(\theta - \hat{\theta})\,/ \Delta \theta$", fontsize=18)
        plt.show()

        out_fname = os.path.splitext(ws_file)[0]+'_pulls.csv'
        with open(out_fname, 'w') as pull_file:
            pull_file.write("label,pull,err\n")
            for idx in range(len(pulls)):
                pull_file.write("%s,%.4f,%.4f\n" % (labels[idx], pulls[idx], pullerr[idx]))

    if doCorrelations:
        corr_matrix, corr_labels = get_corr_matrix(ws)
        ax = sns.heatmap(corr_matrix, annot=True,
                         fmt=".2f", xticklabels=corr_labels, yticklabels=corr_labels)
        plt.yticks(rotation=0) 
        plt.xticks(rotation=45, ha='right') 
        plt.show()

    if doImpacts:
        impacts, imp_labels = get_impact_data(ws)

        impcord = np.argsort(np.max(np.abs(impacts[:, :2]), axis=1))
        simpacts = impacts[impcord]
        slabels = np.array(imp_labels)[impcord]

        out_fname = os.path.splitext(ws_file)[0]+'_impacts.csv'
        with open(out_fname, 'w') as imp_file:
            imp_file.write("label,post_dn,post_up,pre_dn,pre_up\n")
            for idx in range(len(slabels)):
                imp_file.write("%s,%.4f,%.4f,%.4f,%.4f\n" % (slabels[idx], 
                                                             simpacts[idx,0],
                                                             simpacts[idx,1],simpacts[idx,2], simpacts[idx,3]))

        #idxs = np.array([np.where(labels==l) for l in slabels]).flatten()
        #spulls = pulls[idxs]
        #spullerr = pullerr[idxs]

        fig, ax1 = plt.subplots()

       # ax1.errorbar(spulls, range(len(spulls)), 
       #              xerr = spullerr, fmt='o', c='k', markersize=5)

        ax1.set_xlabel(r"$(\theta - \hat{\theta})\,/ \Delta \theta$")
        ax1.set_xlim(-2,2)
        ax1.set_zorder(10)
        ax1.patch.set_visible(False)

        ax2 = ax1.twiny()

        ax2.barh(
            range(len(simpacts)),
            np.asarray(simpacts)[:, 0],
            alpha=0.75,
            linestyle="dashed",
            facecolor="r",
        )
        ax2.barh(
            range(len(simpacts)),
            np.asarray(simpacts)[:, 1],
            alpha=0.75,
            linestyle="dashed",
            facecolor="b",
        )
        ax2.barh(
            range(len(simpacts)),
            np.asarray(simpacts)[:, 2],
            alpha=0.75,
            linestyle="dashed",
            fill=None,
            edgecolor="r",
        )
        ax2.barh(
            range(len(simpacts)),
            np.asarray(simpacts)[:, 3],
            alpha=0.75,
            linestyle="dashed",
            fill=None,
            edgecolor="b",
        )

        #plt.xlim(-0.0, 0.001)
        ax2.set_ylim(-0.5, len(simpacts) - 0.5)
        ax1.plot([-1,-1], [-0.5, len(simpacts) - 0.5], '--k')
        ax1.plot([1,1], [-0.5, len(simpacts) - 0.5], '--k')
        ax2.set_xlim(-15,15)
        #fig.set_size_inches(4, 17.5)
        ax2.set_xlabel("Δµ")
        ax2.set_yticks(range(len(slabels)))
        ax2.set_yticklabels(slabels)

        #ax2.grid()
        plt.show()

if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("-i", "--input-ws", dest="ws_file", default="",
                        help="Input workspace (json - maybe we can support ROOT later?)")
    parser.add_argument("--pulls",
                        action="store_true", dest="doPulls", default=False,
                        help="Run pulls") 
    parser.add_argument("--impacts",
                        action="store_true", dest="doImpacts", default=False,
                        help="Run impacts")
    parser.add_argument("--correlations",
                        action="store_true", dest="doCorrelations", default=False,
                        help="Run correlation matrix")

    args = parser.parse_args()
    
    main(args.ws_file, args.doPulls, args.doCorrelations, args.doImpacts) 
   

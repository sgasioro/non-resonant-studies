#!/usr/bin/env python3

import pandas
from tqdm import tqdm
import uproot

from argparse import ArgumentParser

def main(data_file, output_file):
    print("Converting", data_file, "to", output_file)

    dat_store = pandas.HDFStore(data_file)

    nrows = dat_store.get_storer('df').nrows

    if not nrows:
        print('Unable to get nrows, using shape')
        nrows = dat_store.get_storer('df').shape[0]
    chunksize=100000

    c_for_info = dat_store.select('df', start=0, stop=1)

    tree_dict = {}
    for branch in list(c_for_info.keys()):
        tree_dict[branch] = str(c_for_info[branch].dtype)

    with uproot.recreate(output_file) as f:
        f['control'] = uproot.newtree(tree_dict)
        f['validation'] = uproot.newtree(tree_dict)
        f['sig'] = uproot.newtree(tree_dict)

        for i in tqdm(range(nrows//chunksize+1)):
            chunk = dat_store.select('df',
                                 start=i*chunksize,
                                 stop=(i+1)*chunksize)
            
            chunk = chunk[(abs(chunk['eta_h1']-chunk['eta_h2'])<1.5)]
            
            f['control'].extend(chunk[chunk['kinematic_region']==2].to_dict(orient='list'))
            f['validation'].extend(chunk[chunk['kinematic_region']==1].to_dict(orient='list'))
            f['sig'].extend(chunk[chunk['kinematic_region']==0].to_dict(orient='list'))


if __name__ == '__main__':    
    parser = ArgumentParser()
    parser.add_argument("-d", "--data-file", dest="data_file", default="",
                        help="Input file (h5)")
    parser.add_argument("-o", "--output-file", dest="output_file", default="",
                        help="Output file (ROOT)")


    args = parser.parse_args()
    main(args.data_file, args.output_file)

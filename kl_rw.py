import numpy as np
import uproot

def get_xsec(kl):
    '''
    Taken from lhcxswg twiki
    '''
    return 70.3874-50.4111*kl+11.0595*(kl**2)

def getLambdaWeights(smnr,kl_vals,lambdaFile='data/weight-mHH-from-cHHHp01d0-to-cHHHpx_10GeV_Jul28.root'):
    '''
    Add weights corresponding to the different lambda variations to the df
    - smnr: pandas df
    - kl_vals: list of kl values to add
    - lambdaFile: The file storing the reweighting histogram for truth_mhh 
    '''

    f = uproot.open(lambdaFile)

    for kl in kl_vals:

        if kl == 1: # no need to rw
            continue

        neg = 'n' if kl < 0 else ''
        histName = f'reweight_mHH_1p0_to_{neg}{np.abs(kl)}p0'
        rw = f[histName].allvalues
        edg = f[histName].edges

        idx = np.digitize(smnr['truth_mhh'],edg)

        smnr[f'w_k{kl}'] = get_xsec(kl)/get_xsec(1) * rw[idx] * smnr['mc_sf']


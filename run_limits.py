#!/usr/bin/env python3

from utils import *
from lim_utils import *
from kl_rw import *

import pandas
from tqdm import tqdm
import uproot
import matplotlib.pyplot as plt

from argparse import ArgumentParser

def main(fnames_dat, fnames_sig, yrs,
         doBootstrap, doSyst, doTheoryplusLumi, in_format, label, 
         bins, var, HTcut, cat_var, cat_bounds, kl_vals, save_ws, ws_only):

    full_data_SR = {}
    med_norm = {}
    med_norm_VR = {}
    med_norm_IQR = {}

    sig_SR = {}

    #Setup for categories - include some defaults for cosThetaStar from prev studies
    if cat_var == "cosThetaStar":
        if not cat_bounds:
            cat_bounds = [0, 0.05, 0.1, 0.15, 0.2, 0.25,
                          0.3, 0.4, 0.5, 0.65, 1]
    else:
        if cat_var and not cat_bounds:
            print(cat_var, "not supported without boundaries!")
            return 0

    #Nicole's format has HT, cosThetaStar, and, kl weights, calculated already.
    #Need to do these by hand for RR
    base_columns = ['ntag', 'm_hh_cor', 'm_hh', 'X_wt']
    kl_columns = []
    doRR_rw = False
    if in_format == 'resolved-recon':
        doLumi = True
        base_columns += ['pT_h1_j1', 'pT_h1_j2', 'pT_h2_j1', 'pT_h2_j2']
        if cat_var == "cosThetaStar":
            base_columns += ['pT_h1', 'eta_h1', 'phi_h1', 'm_h1',
                             'pT_h2', 'eta_h2', 'phi_h2', 'm_h2']

        if len(kl_vals) > 0 and list(kl_vals) != [1]:
            kl_columns = ['truth_mhh']
            doRR_rw = True
    else:
        doLumi = False
        base_columns += ['HT']
        if cat_var == "cosThetaStar":
            base_columns += ['cosThetaStar']

        if len(kl_vals) > 0:
            if list(kl_vals) == [1]:
                print("Running for SM")
            else:
                print("Running for kl =", kl_vals) 
            kl_columns = ['w_k%d'%kl for kl in kl_vals if kl != 1]

    if var not in base_columns:
        base_columns += [var]

    #Actually load in the things
    for yr in yrs:
        columns = base_columns + ['NN_d24_weight_bstrap_med_%s' % yr, 'NN_d24_weight_VRderiv_bstrap_med_%s' % yr,
                                  'NN_d24_weight_bstrap_IQR_%s' % yr]

        columns_mc = base_columns + ['mc_sf'] + kl_columns

        full_data_SR[yr] = load(fnames_dat[yr],
                                 columns, 'sig')
        
        #Nicole format has no Xwt cut applied, do it just in case
        full_data_SR[yr] = full_data_SR[yr][full_data_SR[yr]['X_wt']>=1.5]
                        
        f = uproot.open(fnames_dat[yr][0])
        med_norm[yr] = f['NN_norm_bstrap_med_%s' %yr]._fVal
        med_norm_VR[yr] = f['NN_norm_VRderiv_bstrap_med_%s' %yr]._fVal
        med_norm_IQR[yr] = f['NN_norm_bstrap_IQR_%s' %yr]._fVal

        if len(fnames_sig[yr]) > 1:
            print("Not supported!")
            return 0 

        f_sig = uproot.open(fnames_sig[yr][0])
        sig_SR[yr] = f_sig['sig'].pandas.df(columns_mc)

        #Remove systematic variations from RR signal
        if in_format == 'resolved-recon':
            sig_SR[yr] = sig_SR[yr].xs(0, axis = 0, level = 1, drop_level = True)
        sig_SR[yr] = sig_SR[yr][sig_SR[yr]['X_wt']>=1.5]


        if cat_var == "cosThetaStar":
            if in_format == 'resolved-recon':
                full_data_SR[yr].insert(0, 'cosThetaStar', cosThetaStar(full_data_SR[yr]), False)
                sig_SR[yr].insert(0, 'cosThetaStar', cosThetaStar(sig_SR[yr]), False)
            else:
                full_data_SR[yr]['cosThetaStar'] = abs(full_data_SR[yr]['cosThetaStar'])
                sig_SR[yr]['cosThetaStar'] = abs(sig_SR[yr]['cosThetaStar'])

        print(np.sum(sig_SR[yr][sig_SR[yr]['ntag']>=4]['mc_sf']))


    if doSyst:
        print("Running with syst, HT cut", HTcut)
    if doBootstrap:
        print("Running with bootstrap")

    if doRR_rw:
        for yr in yrs:
            print("Adding relevant kl weights to df for", yr)
            #Hardcoded mc_sf fix! Will remove when updated
            bugged_dir = "/eos/user/v/valentem/work/ATLAS/HH/HH4b/grid_outputs/recon/2020-10-02/MC/"
            if fnames_sig[yr][0].find(bugged_dir) != -1: 
                sig_SR[yr]['mc_sf']  *= (get_xsec(1)  / 27.872)

            getLambdaWeights(sig_SR[yr], kl_vals)

    for kl in kl_vals:

        #Sets up class for easy categorization handling
        data_setup = data_prep_ntup(sig_SR, full_data_SR, med_norm, med_norm_VR, 
                                     yrs, bins, col=var, lumi_scale=doLumi, 
                                     norm_IQR=med_norm_IQR, doSysts=doSyst, doBootstrap=doBootstrap, 
                                     doTheoryplusLumi = doTheoryplusLumi, systcutoff=HTcut, kl=kl)

        #A dict with all of the histograms and things needed to make the pyhf model
        dat_for_lim = data_setup.categorize(cat_var, cat_bounds)
        
        #Saves workspace as json (and ROOT if save_ws has .root extension
        if save_ws:
            if len(kl_vals) > 1:
                print("Not ready for this yet!")
                return 0 
            save_wkspace(dat_for_lim, save_ws)
            if ws_only:
                return 1


        #Actually run the limits - set up minimizer class with histograms and run a scan
        find = mu_finder(dat_for_lim)
        exp_lims = find.run_scan()
        print(exp_lims)
        
        #Output as a csv
        out_name = 'exp-lim-smnr-'
        if doSyst:
            out_name += 'systs-HTcut-%d-' % HTcut 
        else:
            out_name += 'stat-only-'

        for yr in yrs:
            out_name+= '%s-' % yr

        if kl == 1:
            out_name+='SM-HH-'
        else:
            out_name+='kl-%d-' % kl
 
        if label:
            out_name+=(label+'-')
        out_name+= 'comb.csv'

        print("Writing to", out_name)
        with open(out_name, 'w') as out_file:
            out_file.write("-2s, -1s, Exp, 1s, 2s\n")
            out_file.write("%.3f,%.3f,%.3f,%.3f,%.3f\n" % (exp_lims[0],
                                                           exp_lims[1], 
                                                           exp_lims[2],
                                                           exp_lims[3],
                                                           exp_lims[4]))

if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("-d", "--data", dest="data_file", default=[], nargs="+",
                        help="Input data filenames")
    parser.add_argument("-s", "--signal", dest="sig_file", default=[], nargs="+",
                        help="Input signal filenames")
    parser.add_argument("-y", "--years", dest="years", default=[], nargs="+",
                        help="Input years")
    parser.add_argument("--stat-only",
                        action="store_true", dest="stat_only", default=False,
                        help="Stat only limits")
    parser.add_argument("--no-bootstrap",
                        action="store_true", dest="no_bootstrap", default=False,
                        help="No bootstrap error")
    parser.add_argument("--format", dest="in_format", default="",
                        help="Nicole's format or resolved-recon")
    parser.add_argument("--HTcut", dest="HTcut", default=300,
                        help="HT (or other syst) cutoff")
    parser.add_argument("-l", "--label", dest="label", default="",
                        help="Label for output")
    parser.add_argument("--categorize", dest="cat_var", default="",
                        help="Variable to categorize in (e.g. cosThetaStar)")
    parser.add_argument("--cat-edges", dest="cat_bounds", default=[], type=float, nargs="+",
                        help = "Category boundaries")
    parser.add_argument("--bins", dest="bins", default=[70], nargs="+", type=float,
                        help = "Bins for m_hh_cor. Single arg = n_bins, list = bin edges.")
    parser.add_argument("--range", dest="bin_range", default=[250, 1250], type=float, nargs="+",
                        help = "Lower and upper bounds for m_hh_cor hist")
    parser.add_argument("--var", dest="var", default="m_hh_cor",
                        help = "Variable on which to run limits")
    parser.add_argument("--kl", dest="kl", default=[1], nargs="+",
                        help = "kappa lambda value(s)")
    parser.add_argument("--save-wkspace", dest="save_ws", default="",
                        help="Save workspace if given output name")
    parser.add_argument("--wkspace-only", 
                        action="store_true", dest="wkspace_only", default=False,
                        help="Only make workspace (don't run limits)")
    parser.add_argument("--doTheoryLumiUnc",
                        action="store_true", dest="doTheoryLumiUnc", default=False,
                        help="Do theory+lumi uncertainty")
    args = parser.parse_args()

    data_file = args.data_file

    #Accept 2016 or 16 type format for years
    yrs = []
    for yr in args.years:
        if len(yr) > 2:
            yrs.append(yr[2:])
        else:
            yrs.append(yr)

    #Match data file names to years (file must contain year)
    fnames_dat = {}
    for yr in yrs:
        yr_farray = []
        for dat_file in data_file:
            if dat_file.find(yr) != -1:
                yr_farray.append(dat_file)
        fnames_dat[yr] = yr_farray

    #Match signal file campaigns to years (file must contain mc16 or MC16 + a, d, e)
    campaigns = {'16' : 'a', '17':'d', '18': 'e'}
    fnames_sig = {}
    for yr in yrs:
        yr_farray = []
        for sig_file in args.sig_file:
            if sig_file.find('mc16%s'%campaigns[yr]) != -1 or sig_file.find('MC16%s'%campaigns[yr]) != -1:
                yr_farray.append(sig_file)         
        fnames_sig[yr] = yr_farray


    #Decide which uncertainties - default is all
    doSyst = not args.stat_only
    doTheoryplusLumi = (not args.stat_only) or args.doTheoryLumiUnc
    doBootstrap = not args.no_bootstrap

    #Get bins from CLI
    if len(args.bins) == 1:
        xlow = args.bin_range[0]
        xhigh = args.bin_range[1]
        n_bins = int(args.bins[0])
        print("Running with", n_bins, "bins from", xlow, "to", xhigh, "for variable", args.var)
        bins = np.linspace(xlow, xhigh, n_bins+1)
    else:
        bins = args.bins
        print("Running with custom bins", bins, "for variable", args.var)

    #Set up kl scan values from CLI
    if len(args.kl) == 1 and args.kl[0] == "all":
        kl_vals = np.arange(-20, 21)
    else:
        kl_vals = np.array([int(kl) for kl in args.kl])

    main(fnames_dat, fnames_sig, yrs, doBootstrap, doSyst, doTheoryplusLumi, args.in_format, args.label, bins, 
         args.var, float(args.HTcut), args.cat_var, args.cat_bounds, kl_vals, args.save_ws, args.wkspace_only)

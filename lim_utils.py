import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import pyhf
import pyhf.contrib.viz.brazil
from tqdm import tqdm
import os
import uproot
import json
import subprocess

import time
from scipy.optimize import root_scalar
from multiprocessing import Pool
from scipy.special import erfinv, erf

from utils import *

def create_model(data, returnWorkspace=False, corr_yrs=False, corr_cat=True, batch_size=None):
    '''
    Create pyhf model from dict of input data (see data_prep_ntup class)
    - data: dict
    - returnWorkspace: if true, return workspace, model, and data instead of just model
    - corr_yrs: correlate systematics between years
    - corr_cat: correlate systematics between categories
    '''

    chans  = []
    if returnWorkspace:
        obs = []

    theoryPlusLumi = False
    for yr in data.keys(): 
        for cat in data[yr].keys():
            #Correlations are tracked through names
            if corr_yrs and corr_cat:
                label=""
            elif corr_yrs and not corr_cat:
                label=cat
            elif corr_cat and not corr_yrs:
                label=yr
            else: #not corr_cat and not corr_yrs
                label=yr+cat

            unshare_label = yr+cat
            mods = []
            if 'stat' in data[yr][cat].keys():
                print("Running stat error")
                mods.append({'name': 'uncorr_bkguncrt'+unshare_label, 'type': 'shapesys','data': data[yr][cat]['stat']})
            if 'Low_HT' in data[yr][cat].keys() and 'High_HT' in data[yr][cat].keys():
                print("Running syst error")
                mods.append({'name': 'low_HT'+label,  'type':'histosys', "data":{"lo_data":data[yr][cat]['Low_HT'][0], "hi_data":data[yr][cat]['Low_HT'][1]}})
                mods.append({'name': 'high_HT'+label, 'type':'histosys', "data":{"lo_data":data[yr][cat]['High_HT'][0],"hi_data":data[yr][cat]['High_HT'][1]}})
            if 'norm' in data[yr][cat].keys():
                print("Running norm error")
                mods.append({'name': 'bkg_norm'+label,'type':'normsys',  "data":{"lo":data[yr][cat]['norm'][0],"hi":data[yr][cat]['norm'][1]}})

            #Add mu as a normfactor
            sig_mods = [{'name': 'mu', 'type': 'normfactor', 'data': None }]
            if data[yr][cat]["Theory+Lumi"]:
                theoryPlusLumi = True
                print("Running theory and lumi")
                sig_mods.append({ "name": "lumi", "type": "lumi", "data": None })
                sig_mods.append({'data': {'hi': 1.05, 'lo': 0.95}, 'name': 'Theory', 'type': 'normsys'})

            chans.append(
                    {
                        'name': 'channel'+unshare_label,
                        'samples': [
                            {
                                'name': 'signal'+unshare_label,
                                'data': data[yr][cat]['signal'],
                                'modifiers': sig_mods,
                            },
                            {
                                'name': 'background'+unshare_label,
                                'data': data[yr][cat]['bkgd'],
                                'modifiers': mods,
                            },
                        ],
                    }
            )
            
            if returnWorkspace:
                obs.append(
                            {
                                   'name': 'channel'+unshare_label,
                                   'data': list(np.array(data[yr][cat]['bkgd']))
                            }
                          )
        
    spec = {'channels' : chans}
    
    if returnWorkspace:
        spec['observations'] = obs
 
        #Unbound mu for ws
        meas_params = [{ "name": "mu", "bounds": [[-1000, 1000000]], "inits":[1.0] }]
        if theoryPlusLumi:
            meas_params.append({ "name":"lumi", "auxdata":[1.0],"sigmas":[0.017], "bounds":[[0.915,1.085]],"inits":[1.0] })

        spec['measurements'] = [   
                                 { 
                                    'name': 'Measurement', 
                                    'config': {
                                        'poi': 'mu', 
                                        'parameters': meas_params 
                                    } 
                                 }
                               ] 
        spec['version'] = '1.0.0'
      

        w = pyhf.Workspace(spec)
        
        m = w.model(
                measurement_name="Measurement",
                modifier_settings={
                    "normsys": {"interpcode": "code4"},
                    "histosys": {"interpcode": "code4p"},
                },
            )
        
        d = w.data(m)
        
        return w,m,d

    else:
        #Unbound mu for model
        params = [{'bounds': [[-1000, 1000000]], 'inits': [1.0], 'name': 'mu'}]
        if theoryPlusLumi:
            params.append({'auxdata': [1.0],
                               'sigmas': [0.017],
                               'bounds': [[0.915, 1.085]],
                               'inits': [1.0], 'name': 'lumi'})

        spec['parameters'] = params 

        return pyhf.Model(spec, batch_size=batch_size)


def save_wkspace(in_data, outname):
    '''
    Function to export workspace
    - in_data: dict (see data_prep_ntup)
    - outname: name of file to write to. Always does json, if .root, do ROOT.
    '''

    print("Saving ws to", outname)

    name, ext = os.path.splitext(os.path.basename(outname))

    w, _, _ = create_model(in_data, batch_size=None, returnWorkspace=True)

    jsname = name+'.json'
    with open(jsname, 'w') as outfile:
        json.dump(w, outfile)

    if ext == '.root':
        print("Converting to RooWorkspace...first use pyhf to go from json to XML+ROOT")
        print("Writing to dir", name)
        print(subprocess.check_output(["mkdir", "-p", name]).decode("utf-8"))
        print(subprocess.check_output(["pyhf", "json2xml", jsname, "--output-dir", name]).decode("utf-8"))
        print("Then convert to RooWorkspace using hist2workspace")
        print(subprocess.check_output(["hist2workspace", name+"/FitConfig.xml"]).decode("utf-8"))
        print("Output in", name+"/config/")


class data_prep_ntup:
    '''
    Data preparation class. Initialize with ntuple info, parameters. Contains
    - prep_data: makes all relevant histograms, given pandas df for each category
    - categorize: splits full pandas df into categories/years
    '''
    #Initialize with full data/signal arrs, norms 
    def __init__(self, arr_sig, arr_back, norm, norm_VR,
                 yrs, bins, **kwargs): 
                 
        self.arr_sig = arr_sig
        self.arr_back = arr_back
        self.norm = norm
        self.norm_VR = norm_VR
        self.yrs = yrs
        self.bins = bins

        prop_defaults = {
                          "scale_sig" : 1., 
                          "eps"  : 1e-20, 
                          "col" : 'm_hh_cor',
                          "doSysts" : True, 
                          "doBootstrap" : True,
                          "doTheoryplusLumi" : True,
                          "norm_IQR" : [], 
                          "lumi_scale" : False, 
                          "systvar" : "HT", 
                          "systcutoff": "300",
                          "kl": 1
                        }


        for (prop, default) in prop_defaults.items():
            setattr(self, prop, kwargs.get(prop, default))

        self.lumi = {'16': 24.6, '17': 43.65, '18': 58.45}

    def prep_data(self, back_chunk, sig_chunk, yr):
        chunk_for_lim = {}
        if self.lumi_scale:
            lumi_yr = self.lumi[yr]
        else:
            lumi_yr = 1.

        if self.kl == 1:
            w_str = 'mc_sf'
        else:
            w_str = 'w_k%d' % self.kl

        n_sig, _ = np.histogram(sig_chunk[sig_chunk['ntag']>=4][self.col], bins=self.bins,
                                weights=sig_chunk[sig_chunk['ntag']>=4][w_str]*lumi_yr)


        nom, lowVRw, highVRw, lowVRi, highVRi = systs(back_chunk, self.norm[yr], self.norm_VR[yr], yr, self.bins,
                                                      col=self.col, var=self.systvar, cutoff=self.systcutoff)

        norm_syst = abs(self.norm_VR[yr] - self.norm[yr]) / self.norm[yr]

        if self.doSysts:
            chunk_for_lim['norm'] = [1-norm_syst, 1+norm_syst]
            chunk_for_lim['Low_HT'] = [list(lowVRw+self.eps), list(lowVRi+self.eps)]
            chunk_for_lim['High_HT'] = [list(highVRw+self.eps), list(highVRi+self.eps)]

        chunk_for_lim['Theory+Lumi'] = self.doTheoryplusLumi

        chunk_for_lim['scale'] = self.scale_sig
        chunk_for_lim['signal'] = list(n_sig*self.scale_sig+self.eps)
        chunk_for_lim['bkgd'] = list(nom+self.eps)

        if self.doBootstrap:
            chunk_for_lim['stat'] = list(calc_bstrap(back_chunk, self.col,
                                                     yr, self.norm[yr], self.norm_IQR[yr], self.bins)+self.eps)
        else:
            chunk_for_lim['stat'] = list(weighted_err(back_chunk[back_chunk['ntag']==2][self.col],
                                                      back_chunk[back_chunk['ntag']==2]['NN_d24_weight_bstrap_med_%s'%yr]*self.norm[yr],
                                                      self.bins)+self.eps)
        
        return chunk_for_lim

    def categorize(self, cat_var="", bounds=[]):
        dat_for_lim = {}
        if cat_var and bounds:
            print("Categorizing in", cat_var, "with boundaries", bounds)
            for yr in self.yrs:
                dat_for_lim[yr] = {}
                for b_idx in range(len(bounds)-1):
                    back_chunk = self.arr_back[yr][(self.arr_back[yr][cat_var] > bounds[b_idx]) &
                                                   (self.arr_back[yr][cat_var] <= bounds[b_idx+1])]
                    sig_chunk = self.arr_sig[yr][(self.arr_sig[yr][cat_var] > bounds[b_idx]) &
                                                  (self.arr_sig[yr][cat_var] <= bounds[b_idx+1])]

                    dat_for_lim[yr]['cat_%d'%b_idx] = self.prep_data(back_chunk, sig_chunk, yr)

        else:
            print("Splitting into years only for", self.yrs)
            for yr in self.yrs:
                back_chunk = self.arr_back[yr]
                sig_chunk = self.arr_sig[yr]
                dat_for_lim[yr] = {}
                dat_for_lim[yr][''] = self.prep_data(back_chunk, sig_chunk, yr)

        return dat_for_lim

#Quantile fn for standard normal distribution (inv CDF)
def probit(p):
    return np.sqrt(2)*erfinv(2*p - 1)

#CDF of standard normal distribution
def norm(p):
    return 1/2*(1+erf(p/np.sqrt(2)))

#Guess at bands given med mu (arXiv:1007.1727 eq. 88/89, mu' = 0, modified for CLs)
#https://indico.cern.ch/event/126652/contributions/1343592/attachments/80222/115004/Frequentist_Limit_Recommendation.pdf
#eq 12 and 14
def quick_bands(mu, alpha = 0.05):
    s = mu/probit(1-0.5*(alpha))
    guesses = [(s*(probit(1-alpha*norm(N))+N)) for N in [-2, -1, 0, 1, 2]]
    return guesses


class mu_finder:
    def __init__(self, in_data):
        model = create_model(in_data)
        data = []
        count = 0
        for yr in in_data.keys():
            for cat in in_data[yr].keys():
                if count == 0:
                    scale_val = in_data[yr][cat]['scale']
                data+=in_data[yr][cat]['bkgd']
                count+=1

        if count != len(model.spec['channels']):
            print("Error - mismatch in data and background re: channels!")
            return 0

        data = data+ model.config.auxdata

        self.data = data
        self.model = model
        self.in_data = in_data
        self.scale = scale_val
        
    def test_mu(self, mu_test, idx=2, alpha=0.05):
        result = pyhf.infer.hypotest(mu_test, self.data, self.model, qtilde=False, return_expected_set=True)
        CLs_val = result[1][idx]
        print("Param: %d, mu: %.3f, CLs: %.3f" % (idx, mu_test, CLs_val))
        return CLs_val-alpha
    
    def min_for_lim(self, args):
        idx,alpha,guess,xtol,window = args
        return root_scalar(self.test_mu, bracket=(guess*(1-window),guess*(1+window)), 
                           args=(idx, alpha), xtol =xtol)

    def run_scan(self, alpha=0.05,xtol=1e-2, threads=4):
        pyhf.set_backend("numpy")

        t0 = time.time()

        #Calculate qmu, asimov
        init_pars = self.model.config.suggested_init()
        fixed_params = self.model.config.suggested_fixed()
        par_bounds = self.model.config.suggested_bounds()
        par_bounds[self.model.config.poi_index] = [-40.0, 40.0]

        init_mu = init_pars[self.model.config.poi_index]

        qmu = pyhf.infer.test_statistics.qmu(init_mu, self.data, self.model, 
                                             par_bounds=par_bounds,
                    init_pars=init_pars, fixed_params=fixed_params)

        #Guess - sigma from eq 31 in arXiv:1007.1727, then see quick_bands 
        med_guess = np.sqrt(init_mu**2/qmu)*probit(1-0.5*alpha)

        #Some weird numerical instabilities for qmu sometimes - rescaling signal seems to fix usually
        if med_guess > 100:
            print("Unexpectedly large med guess (%.2f), verifying by rescaling signal" % med_guess)
            in_test = self.in_data.copy()
            for yr in in_test.keys():
                for cat in in_test[yr].keys():
                    in_test[yr][cat]['signal'] = list(np.array(in_test[yr][cat]['signal'])*20.)

            test_model = create_model(in_test)
            test_data = []
            count = 0
            for yr in in_test.keys():
                for cat in in_test[yr].keys():
                    if count == 0:
                        scale_val = in_test[yr][cat]['scale']
                    test_data+=in_test[yr][cat]['bkgd']
                    count+=1

            if count != len(test_model.spec['channels']):
                print("Error - mismatch in data and background re: channels!")
                return 0
            
            test_data = test_data+ test_model.config.auxdata

            init_pars_test = test_model.config.suggested_init()
            fixed_params_test = test_model.config.suggested_fixed()
            par_bounds_test = test_model.config.suggested_bounds()
            par_bounds_test[test_model.config.poi_index] = [-40.0, 40.0]

            init_mu_test = init_pars_test[test_model.config.poi_index]

            qmu_test = pyhf.infer.test_statistics.qmu(init_mu_test, test_data, test_model,
                                                 par_bounds=par_bounds_test,
                        init_pars=init_pars_test, fixed_params=fixed_params_test)

            med_guess_test = np.sqrt(init_mu_test**2/qmu_test)*probit(1-0.5*alpha)*20.

            if abs(med_guess_test-med_guess)/med_guess > 0.5:
                print("Initial guess was wrong, subbing in new guess")
                med_guess = med_guess_test


        band_guess = quick_bands(med_guess)

        scale = self.scale 
        print("Initial guess for bands (mu guess = %.2f):" % init_mu)
        print(np.array(band_guess)*scale)

        print("Beginning optimization, mu valid to %.2f" % (xtol*scale))

        print("Finding med_mu for alpha = %.2f" % alpha)
        try:
            min_val = root_scalar(self.test_mu, bracket=(band_guess[0],band_guess[4]),
                                  args=(2, alpha), xtol =xtol)
        except:
            print("Looks like initial guess was bad? Expanding for one more try")
            min_val = root_scalar(self.test_mu, bracket=(band_guess[0]/2.,band_guess[4]*2.),
                                  args=(2, alpha), xtol =xtol)

        s_guess = quick_bands(min_val.root)
        print("Guess for bands with proper med:")
        print(np.array(s_guess)*scale)

        print("Refining bands in %d threads" % threads)
        p = Pool(threads)
        args = [(i, alpha, s_guess[i], xtol, 0.05) for i in [0,1,3,4]]
        band_out = p.map(self.min_for_lim,args)

        final_bands = [band_out[0].root*scale,
                       band_out[1].root*scale,
                       min_val.root*scale,
                       band_out[2].root*scale,
                       band_out[3].root*scale]
        print("Final bands:")
        print(final_bands)

        calls = 0
        for out in band_out:
            calls+=out.function_calls
        calls+=min_val.function_calls

        t1 = time.time()
        interval = t1-t0
        print("Finished in", interval//60,"min", interval % 60, "s")
        print("%d function calls" % calls)
        return final_bands

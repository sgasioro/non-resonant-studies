#!/usr/bin/env python3

import glob
import pandas as pd
import numpy as np

from kl_rw import get_xsec

from argparse import ArgumentParser

def collect(pattern, with_xs=False, return_df=False):
    filelist = glob.glob(pattern)
    w_card_idx  = pattern.find('*')
    pre_w_card = pattern[:w_card_idx]
    post_w_card = pattern[w_card_idx+1:]
    if post_w_card[0] != '-':
        post_w_card = '-'+post_w_card
    if pre_w_card[-1] != '-':
        pre_w_card = pre_w_card +'-'

    kl_vals = []
    for f in filelist:
        if f.find('kl') != -1:
            kl_vals.append(int(f[f.find('kl')+3:f.find(post_w_card)]))
        elif f == pre_w_card+'SM-HH'+post_w_card:
            kl_vals.append(1)
    kl_vals=sorted(kl_vals)

    for i in range(len(kl_vals)):
        kl = kl_vals[i]

        if with_xs:
            xs = get_xsec(kl)/1000.
        else:
            xs = 1.

        if i == 0:
            if kl != 1:
                lim_df = pd.read_csv((pre_w_card+'kl-%d'+post_w_card) % (kl))*xs
            else:
                lim_df = pd.read_csv(pre_w_card+'SM-HH'+post_w_card)*xs
        else:
            if kl == 1:
                lim_df = lim_df.append(pd.read_csv(pre_w_card+'SM-HH'+post_w_card)*xs, ignore_index=True)
            else:
                lim_df= lim_df.append(pd.read_csv((pre_w_card+'kl-%d'+post_w_card) % (kl))*xs, ignore_index=True)

    lim_df =lim_df.set_index(np.asarray(kl_vals))

    print("Writing to", pre_w_card+post_w_card[1:])
    lim_df.rename(columns={" -1s": "-1s", " Exp": "Exp", 
                           " 1s": "1s", " 2s":"2s"}).to_csv(pre_w_card+post_w_card[1:],index_label='kl', float_format='%.3f')

    print(lim_df)

    if return_df:
        return lim_df

def main(pattern, with_xs):
    collect(pattern, with_xs=with_xs)

if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("--pattern", dest="pattern", required=True,
                        help="Pattern of files with kl chunk wildcarded (with *). Need to escape (\*) when providing. Note: position matters!")
    parser.add_argument("--with-xs",
                        action="store_true", dest="with_xs", default=False,
                        help="Return kl limits on xs(pp->HH) rather than mu.")
    args = parser.parse_args()

    pattern = args.pattern
    with_xs = args.with_xs
    print("Collecting files of form", pattern)
    if with_xs:
        print("Saving limits on xs(pp->HH)")
    else:
        print("Saving limits on mu")

    main(pattern, with_xs)
   



